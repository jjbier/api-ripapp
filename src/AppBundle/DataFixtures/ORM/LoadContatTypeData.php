<?php
/**
 * This file (LoadContatTypeData.php) is part of the api-ripapp project.
 *
 * 2014 (c) jjbier@gamil.com.
 * Created by Xabier Fernández Rodríguez <jjbier@gmail.com>
 * Date: 16/03/16 - 20:31
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ContactType;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadContatTypeData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadContatTypeData implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $data = self::getConstants();
        foreach($data as $key=>$value){
            $contactType = new ContactType();
            $contactType->setValue(strtoupper($key));
            $manager->persist($contactType);
        }
        $manager->flush();
    }

    private static $constCacheArray = NULL;

    private static function getConstants() {
        if (self::$constCacheArray == NULL) {
            self::$constCacheArray = [];
        }
        $calledClass = __CLASS__;
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new \ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }

    /** Members of a Family  */
    const father = 1;
    const mother = 2;
    const son = 3;
    const daughter = 4;
    const brother = 5;
    const sister = 6;
    const grandfather = 7;
    const granddad = 8;
    const grandmother = 9;
    const grandma = 10;
    const grandson = 11;
    const granddaughter = 12;
    const grandchild = 13;
    const uncle = 14;
    const aunt = 15;
    const cousin = 16;
    const nephew = 17;
    const niece = 18;
    const boyfriend = 19;
    const girlfriend = 20;
    const fiance = 21;
    const fiancee = 22;
    const bride = 23;
    const groom = 24, bridegroom = 24;
    const wife = 25;
    const husband = 26;
    const spouse = 27;
    const fatherInLaw = 28;
    const motherInLaw = 29;
    const parentsInLaw = 30;
    const sonInLaw = 31;
    const daughterInLaw = 32;
    const brotherInLaw = 33;
    const sisterInLaw = 34;
    const godfather = 35;
    const godmother = 36;
    const godson = 37;
    const goddaughter = 38;
    const godchild = 39;

    /** Buddy list. */
    const friend = 40;
    const workFriend = 41;
    const familyFriend = 42;
}