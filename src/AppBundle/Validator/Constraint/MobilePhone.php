<?php
/**
 * This file (MobilePhone.php) is part of the api-ripapp project.
 *
 * 2014 (c) jjbier@gamil.com.
 * Created by Xabier Fernández Rodríguez <jjbier@gmail.com>
 * Date: 12/03/16 - 19:08
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace AppBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class MobilePhone extends Constraint
{
    public $message = 'The "%number%" is not a valid mobile phone number';
    public $format  = '/^(\+\d{1,3}[- ]?)?\d{10}$/';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}