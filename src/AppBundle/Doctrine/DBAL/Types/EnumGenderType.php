<?php
/**
 * Created by PhpStorm.
 * User: xabier
 * Date: 9/03/16
 * Time: 22:36
 */

namespace AppBundle\Doctrine\DBAL\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Class EnumGenderType
 * @package AppBundle\Doctrine\DBAL\Types
 */
class EnumGenderType extends EnumType
{
    protected $name = 'enum_gender';
    protected $values = array('male', 'female', 'other');

    const ENUM_GENDER = 'enum_gender';
    const GENDER_MAN = 'male';
    const GENDER_FEMALE = 'female';
    const GENDER_OTHER = 'other';
}
