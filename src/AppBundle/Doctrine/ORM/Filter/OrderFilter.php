<?php
/**
 * This file (OrderFilter.php) is part of the api-ripapp project.
 *
 * 2014 (c) jjbier@gamil.com.
 * Created by Xabier Fernández Rodríguez <jjbier@gmail.com>
 * Date: 12/03/16 - 20:04
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace AppBundle\Doctrine\ORM\Filter;

use Dunglas\ApiBundle\Doctrine\Orm\Filter\OrderFilter as BaseOrderFilter;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class OrderFilter
 * @package AppBundle\Doctrine\ORM\Filter
 */
class OrderFilter extends BaseOrderFilter
{
    protected function extractProperties(Request $request)
    {
        return parent::extractProperties($request);
    }
}