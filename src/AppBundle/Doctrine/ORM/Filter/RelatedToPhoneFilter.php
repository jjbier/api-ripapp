<?php
/**
 * This file (RelatedToPhoneFilter.php) is part of the api-ripapp project.
 *
 * 2014 (c) jjbier@gamil.com.
 * Created by Xabier Fernández Rodríguez <jjbier@gmail.com>
 * Date: 16/03/16 - 21:51
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace AppBundle\Doctrine\ORM\Filter;


use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Dunglas\ApiBundle\Api\ResourceInterface;
use Dunglas\ApiBundle\Doctrine\Orm\Filter\AbstractFilter;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ManagerRegistry;

class RelatedToPhoneFilter extends AbstractFilter
{


    public function apply(ResourceInterface $resource, QueryBuilder $queryBuilder, Request $request)
    {
        $entityClass = $resource->getEntityClass();
        // TODO select user DateTimeZone
        $dtz = new \DateTimeZone("UTC");
        $now = new \DateTime(date("Y-m-d"), $dtz);
        $startDate = $now->format("Y-m-d")." 00:00:00";
        $endDate = $now->format("Y-m-d")." 23:59:50";

        if($entityClass != 'AppBundle\Entity\Person' && $entityClass != 'AppBundle\Entity\PersonContact'){
            return ;
        }


        foreach ($this->extractProperties($request) as $property => $values) {
            if ($property == "mobilPhoneRelatedTo") {
                $mobilPhones = explode(";", $values);

                $queryBuilder
                        ->andWhere($queryBuilder->expr()->between('o.deathDate', ':startDate',':endDate'))
                        ->andWhere($queryBuilder->expr()->in('o.mobilPhone', ':mobilPhones'))
                        ->setParameter('startDate', $startDate)
                        ->setParameter('endDate', $endDate)
                        ->setParameter('mobilPhones', $mobilPhones)
                    ;

            }
        }

        if($entityClass == 'AppBundle\Entity\PersonContact') {
            $queryBuilder
                ->leftJoin('o.relatedTo', 'rt')
                ->andWhere($queryBuilder->expr()->between('rt.deathDate', ':startDate',':endDate'))
                ->setParameter('startDate', $startDate)
                ->setParameter('endDate', $endDate)
                ;
        }
    }

    public function getDescription(ResourceInterface $resource)
    {
        $description['mobilPhoneRelatedTo'] = [
            'property' => 'mobilPhoneRelatedTo',
            'type' => 'string',
            'required' => false,
            'description' => 'Mobile numbers separated by \';\'. Example +529116216158;+068687065774',
        ];

        return $description;
    }

}