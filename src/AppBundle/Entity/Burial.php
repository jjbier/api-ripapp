<?php
/**
 * Created by PhpStorm.
 * User: xabier
 * Date: 22/02/16
 * Time: 22:11
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Validator\Constraints as Assert;
use Dunglas\ApiBundle\Annotation\Iri;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Burial.
 * the action or practice of interring a dead body.
 *
 * @package AppBundle\Entity
 *
 * @Entity()
 * @Table(name="burials")
 */
class Burial
{
    /**
     * @var string the resource identify
     * @Id
     * @Column(type="string")
     * @GeneratedValue(strategy="UUID")
     * @Groups({"default"})
     */
    private $id;

    /**
     * @var \DateTime A date value in ISO 8601 date format.
     * @Column(type="datetime" )
     * @Assert\NotNull()
     * @Assert\DateTime()
     * @Iri("https://schema.org/Date")
     * @Groups({"default"})
     */
    private $date;

    /**
     * @var Place The Place
     *
     * @ManyToOne(targetEntity="Place")
     * @Assert\NotNull()
     * @Iri("http://schema.org/Place")
     * @Groups({"default"})
     */
    private $burialPlace;

    /**
     * @var Place The Place
     *
     * @ManyToOne(targetEntity="Place")
     * @Assert\NotNull()
     * @Iri("http://schema.org/Place")
     * @Groups({"default"})
     */
    private $liturgyPlace;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return Place
     */
    public function getBurialPlace()
    {
        return $this->burialPlace;
    }

    /**
     * @param Place $burialPlace
     */
    public function setBurialPlace($burialPlace)
    {
        $this->burialPlace = $burialPlace;
    }

    /**
     * @return Place
     */
    public function getLiturgyPlace()
    {
        return $this->liturgyPlace;
    }

    /**
     * @param Place $liturgyPlace
     */
    public function setLiturgyPlace($liturgyPlace)
    {
        $this->liturgyPlace = $liturgyPlace;
    }
}
