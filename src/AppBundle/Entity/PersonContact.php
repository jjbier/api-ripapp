<?php
/**
 * This file (PersonContact.php) is part of the api-ripapp project.
 *
 * 2014 (c) jjbier@gamil.com.
 * Created by Xabier Fernández Rodríguez <jjbier@gmail.com>
 * Date: 12/03/16 - 12:15
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Validator\Constraints as Assert;
use Dunglas\ApiBundle\Annotation\Iri;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class PersonContact
 * @package AppBundle\Entity
 *
 * @Entity()
 * @Table(name="persons_contacts")
 * @Iri("http://schema.org/PersonContact")
 */
class PersonContact
{

    /**
     * @var string the resource identify
     *
     * @Id()
     * @Column(type="string")
     * @GeneratedValue(strategy="UUID")
     * @Groups({"default"})
     */
    private $id;

    /**
     * @var Person A person (alive, dead, undead, or fictional).
     * @ManyToOne(targetEntity="Person")
     * @Assert\NotNull()
     * @Iri("https://schema.org/Person")
     * @Groups({"default"})
     */
    private $relatedTo;

    /**
     * @var ContactType A contact point
     *
     * @ManyToOne(targetEntity="ContactType")
     * @Assert\NotNull()
     * @Iri("https://schema.org/ContactPoint")
     * @Groups({"default"})
     */
    private $contactType;

    /**
     * @var string The person phone number.
     *
     * @Column(name="mobil_phone")
     * @Assert\NotNull()
     * @Iri("https://schema.org/telephone")
     * @Groups({"default"})
     */
    private $mobilPhone;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Person
     */
    public function getRelatedTo()
    {
        return $this->relatedTo;
    }

    /**
     * @param Person $relatedTo
     */
    public function setRelatedTo($relatedTo)
    {
        $this->relatedTo = $relatedTo;
    }

    /**
     * @return ContactType
     */
    public function getContactType()
    {
        return $this->contactType;
    }

    /**
     * @param ContactType $contactType
     */
    public function setContactType($contactType)
    {
        $this->contactType = $contactType;
    }

    /**
     * @return string
     */
    public function getMobilPhone()
    {
        return $this->mobilPhone;
    }

    /**
     * @param string $mobilPhone
     */
    public function setMobilPhone($mobilPhone)
    {
        $this->mobilPhone = $mobilPhone;
    }
}