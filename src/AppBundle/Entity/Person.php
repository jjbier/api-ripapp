<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Dunglas\ApiBundle\Annotation\Iri;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraint\MobilePhone as AssertMobilePhone;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * A person (alive, dead, undead, or fictional).
 *
 * Documentation on Schema.org
 * @see http://schema.org/Person
 *
 * @Entity()
 * @Table(name="persons")
 * @Iri("http://schema.org/Person")
 */
class Person
{
    const GENDER_MAN = 'male';
    const GENDER_FEMALE = 'female';
    const GENDER_OTHER = 'other';

    /**
     * @var string the resource identify
     *
     * @Id()
     * @Column(type="string")
     * @GeneratedValue(strategy="UUID")
     * @Groups({"default"})
     */
    private $id;

    /**
     * @var string Given name. The first name of a Person.
     *             This can be used along with familyName instead of the name property.
     *
     * @Column(type="string")
     * @Assert\NotNull()
     * @Iri("https://schema.org/givenName")
     * @Groups({"default"})
     */
     private $givenName;

    /**
     *
     * @var string Family name. The last name of an Person.
     *             This can be used along with givenName instead of the name property.
     * @Column(type="string")
     * @Assert\NotNull()
     * @Iri("http://schema.org/familyName")
     * @Groups({"default"})
     */
    private $familyName;

    /**
     * @var string The surnames for Person
     *              An additional name for a Person, can be used for a middle name.
     * @Column(type="string", nullable=true)
     * @Assert\NotNull()
     * @Iri("http://schema.org/additionalName")
     * @Groups({"default"})
     */
    private $additionalName;

    /**
     * @var PostalAddress Physical address of the item.
     *
     * @ManyToOne(targetEntity="PostalAddress")
     * @Assert\NotNull()
     * @Iri("http://schema.org/PostalAddress")
     * @Groups({"default"})
     */
    private $address;

    /**
     * @var \DateTime Date of birth.
     *
     * @Column(type="date")
     * @Assert\NotNull()
     * @Assert\DateTime()
     * @Assert\LessThanOrEqual("today UTC")
     * @Assert\Expression(
     *     "this.birthDate <= this.deathDate",
     *     message="The birth date should be less or equal to death date!"
     * )
     * @Iri("https://schema.org/birthDate")
     * @Groups({"default"})
     */
    private $birthDate;

    /**
     * @var Place The place where the person was born.
     *
     * @ManyToOne(targetEntity="Place")
     * @Iri("https://schema.org/birthPlace")
     * @Groups({"default"})
     */
    private $birthPlace;

    /**
     * @var \DateTime Date of death.
     *
     * @Column(type="date")
     * @Assert\NotNull()
     * @Assert\DateTime()
     * @Assert\LessThanOrEqual("today UTC")
     * @Assert\Expression(
     *     "this.deathDate >= this.birthDate",
     *     message="The dead date should be less or equal to birth date!"
     * )
     * @Iri("https://schema.org/deathDate")
     * @Groups({"default"})
     */
    private $deathDate;

    /**
     * @var Place The place where the person died.
     * @ManyToOne(targetEntity="Place")
     * @Iri("https://schema.org/birthPlace")
     * @Groups({"default"})
     */
    private $deathPlace;

    /**
     * @var Burial The place where the person is burial, entombment.
     * @ManyToOne(targetEntity="Burial")
     * @Groups({"default"})
     */
    private $entombment;

    /**
     * @var Mourning The place where the person is mourning.
     *
     * @ManyToOne(targetEntity="Mourning")
     * @Groups({"default"})
     */
    private $mourning;

    /**
     * @var string The person phone number.
     *
     * @Column(name="mobil_phone")
     * @Assert\NotNull()
     * @AssertMobilePhone()
     * @Iri("https://schema.org/telephone")
     * @Groups({"default"})
     */
    private $mobilPhone;

    /**
     * @var string The person gender.
     *             The valid values are [male, female, other]
     *
     * @Column(type="enum_gender")
     * @Assert\NotNull()
     * @Iri("https://schema.org/gender")
     * @Groups({"default"})
     */
    private $gender;

    /**
     * @var Organization An organization such as a school,
     *                   NGO, corporation, club, etc.
     *
     * @ManyToOne(targetEntity="Organization")
     * @JoinColumn(name="organization_id", referencedColumnName="id")
     * @Iri("https://schema.org/Organization")
     * @Groups({"default"})
     */
    private $organization;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getGivenName()
    {
        return $this->givenName;
    }

    /**
     * @param string $givenName
     */
    public function setGivenName($givenName)
    {
        $this->givenName = $givenName;
    }

    /**
     * @return string
     */
    public function getFamilyName()
    {
        return $this->familyName;
    }

    /**
     * @param string $familyName
     */
    public function setFamilyName($familyName)
    {
        $this->familyName = $familyName;
    }

    /**
     * @return string
     */
    public function getAdditionalName()
    {
        return $this->additionalName;
    }

    /**
     * @param string $additionalName
     */
    public function setAdditionalName($additionalName)
    {
        $this->additionalName = $additionalName;
    }

    /**
     * @return PostalAddress
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param PostalAddress $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return mixed
     */
    public function getBirthPlace()
    {
        return $this->birthPlace;
    }

    /**
     * @param mixed $birthPlace
     */
    public function setBirthPlace($birthPlace)
    {
        $this->birthPlace = $birthPlace;
    }

    /**
     * @return \DateTime
     */
    public function getDeathDate()
    {
        return $this->deathDate;
    }

    /**
     * @param \DateTime $deathDate
     */
    public function setDeathDate($deathDate)
    {
        $this->deathDate = $deathDate;
    }

    /**
     * @return mixed
     */
    public function getDeathPlace()
    {
        return $this->deathPlace;
    }

    /**
     * @param mixed $deathPlace
     */
    public function setDeathPlace($deathPlace)
    {
        $this->deathPlace = $deathPlace;
    }

    /**
     * @return mixed
     */
    public function getEntombment()
    {
        return $this->entombment;
    }

    /**
     * @param mixed $entombment
     */
    public function setEntombment($entombment)
    {
        $this->entombment = $entombment;
    }

    /**
     * @return mixed
     */
    public function getMourning()
    {
        return $this->mourning;
    }

    /**
     * @param mixed $mourning
     */
    public function setMourning($mourning)
    {
        $this->mourning = $mourning;
    }

    /**
     * @return string
     */
    public function getMobilPhone()
    {
        return $this->mobilPhone;
    }

    /**
     * @param string $mobilPhone
     */
    public function setMobilPhone($mobilPhone)
    {
        $this->mobilPhone = $mobilPhone;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
    }
}
