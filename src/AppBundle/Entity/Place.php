<?php
/**
 * Created by PhpStorm.
 * User: xabier
 * Date: 22/02/16
 * Time: 22:02
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\GeneratedValue;
use Dunglas\ApiBundle\Annotation\Iri;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Place.
 * Entities that have a somewhat fixed, physical extension.
 * @package AppBundle\Entity
 *
 * @Iri("http://schema.org/Place")
 *
 * @Entity()
 * @Table(name="places")
 *
 */
class Place
{
    /**
     * @var int
     *
     * @Id()
     * @Column(type="string")
     * @GeneratedValue(strategy="UUID")
     * @Groups({"default"})
     */
    private $id;

    /**
     * @var PostalAddress Physical address of the item.
     *
     * @ManyToOne(targetEntity="PostalAddress")
     * @Assert\NotNull()
     * @Iri("http://schema.org/PostalAddress")
     * @Groups({"default"})
     */
    private $address;

    /**
     * @var GeoCoordinates The geo coordinates of the place.
     *
     * @ManyToOne(targetEntity="GeoCoordinates")
     * @JoinColumn(name="geo_id", referencedColumnName="global_location_number")
     * @Iri("http://schema.org/GeoCoordinates")
     * @Groups({"default"})
     */
    private $geo;

    /**
     * @var string A URL to a map of the place.
     * @Column(type="string", name="has_map")
     * @Iri("http://schema.org/Map")
     * @Groups({"default"})
     */
    private $hasMap;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PostalAddress
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return GeoCoordinates
     */
    public function getGeo()
    {
        return $this->geo;
    }

    /**
     * @return string
     */
    public function getHasMap()
    {
        return $this->hasMap;
    }
}