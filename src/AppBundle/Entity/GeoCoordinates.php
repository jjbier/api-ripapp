<?php
/**
 * This file (GeoCoordinates.php) is part of the api-ripapp project.
 *
 * 2014 (c) jjbier@gamil.com.
 * Created by Xabier Fernández Rodríguez <jjbier@gmail.com>
 * Date: 12/03/16 - 13:47
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\GeneratedValue;
use Dunglas\ApiBundle\Annotation\Iri;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class GeoCoordinates
 * @package AppBundle\Entity
 * @Entity()
 * @Table(name="geo_coordinates")
 */
class GeoCoordinates
{
    /**
     * @var string The Global Location Number
     *             (GLN, sometimes also referred to as International Location Number or ILN)
     *             of the respective organization, person, or place.
     *             The GLN is a 13-digit number used to identify parties and physical locations.
     * @Id()
     * @Column(type="string", name="global_location_number")
     * @Iri("http://schema.org/globalLocationNumber")
     * @Groups({"default"})
     */
    private $id;

    /**
     * @var integer The elevation of a location (WGS 84).
     *
     * @Column(name="elevation", type="bigint", nullable=true)
     * @Iri("http://schema.org/elevation")
     * @Groups({"default"})
     */
    private $elevation;

    /**
     * @var integer The latitude of a location. For example 37.42242 (WGS 84).
     *
     * @Column(name="latitude", type="bigint")
     * @Assert\NotNull()
     * @Iri("http://schema.org/latitude")
     * @Groups({"default"})
     */
    private $latitude;

    /**
     * @var integer The longitude of a location. For example -122.08585 (WGS 84).
     *
     * @Column(name="longitude", type="bigint")
     * @Assert\NotNull()
     * @Iri("http://schema.org/longitude")
     * @Groups({"default"})
     */
    private $longitude;

    /**
     * @var integer The radius in kilometers around the specified location.
     *
     * @Column(name="accuracy_radius", type="bigint", nullable=true)
     * @Groups({"default"})
     */
    private $accuracyRadius;

    /**
     * @var integer The metro code associated with the IP address.
     * These are only available for addresses in the US.
     *
     * @Column(name="metro_code", type="integer", nullable=true)
     * @Groups({"default"})
     */
    private $metroCode;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getElevation()
    {
        return $this->elevation;
    }

    /**
     * @return int
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return int
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return int
     */
    public function getAccuracyRadius()
    {
        return $this->accuracyRadius;
    }

    /**
     * @return int
     */
    public function getMetroCode()
    {
        return $this->metroCode;
    }
}