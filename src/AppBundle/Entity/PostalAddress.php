<?php
/**
 * This file (PostalAddress.php) is part of the api-rip-app project.
 *
 * 2014 (c) jjbier@gamil.com.
 * Created by Xabier Fernández Rodríguez <jjbier@gmail.com>
 * Date: 24/02/14 - 10:22
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Table;
use Dunglas\ApiBundle\Annotation\Iri;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * The mailing address.
 *
 * @Entity()
 * @Table(name="postal_address")
 * @Iri("http://schema.org/PostalAddress")
 */
class PostalAddress
{
    /**
     * @var int the resource identify
     *
     * @Id()
     * @Column(type="string")
     * @GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string The country. For example, USA.
     *             You can also provide the two-letter ISO 3166-1 alpha-2 country code.
     *
     * @see http://en.wikipedia.org/wiki/ISO_3166-1
     *
     * @Column(type="string", name="address_country")
     * @Assert\NotNull()
     * @Assert\Country()
     * @Iri("http://schema.org/addressCountry")
     * @Groups({"default"})
     */
    private $addressCountry;

    /**
     * @var string The locality.
     *             For example, Mountain View.
     *
     * @Column(type="string", name="address_locality")
     * @Assert\NotNull()
     * @Iri("http://schema.org/addressLocality")
     * @Groups({"default"})
     */
    private $addressLocality;

    /**
     * @var string The region.
     *             For example, CA.
     *
     * @Column(type="string", name="address_region", nullable=true)
     * @Iri("http://schema.org/addressRegion")
     * @Groups({"default"})
     */
    private $addressRegion;

    /**
     * @var string The postal code.
     *             For example, 94043.
     *
     * @Column(type="string", name="postal_code", nullable=true)
     * @Iri("http://schema.org/postalCode")
     * @Groups({"default"})
     */
    private $postalCode;

    /**
     * @var string The street address.
     *             For example, 1600 Amphitheatre Pkwy.
     *
     * @Column(type="string", name="street_address")
     * @Assert\NotNull()
     * @Iri("http://schema.org/streetAddress")
     * @Groups({"default"})
     */
    private $streetAddress;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAddressCountry()
    {
        return $this->addressCountry;
    }

    /**
     * @return string
     */
    public function getAddressLocality()
    {
        return $this->addressLocality;
    }

    /**
     * @return string
     */
    public function getAddressRegion()
    {
        return $this->addressRegion;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }
}