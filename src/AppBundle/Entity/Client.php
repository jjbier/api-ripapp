<?php
/**
 * This file (Client.php) is part of the api-ripapp project.
 *
 * 2014 (c) jjbier@gamil.com.
 * Created by Xabier Fernández Rodríguez <jjbier@gmail.com>
 * Date: 30/03/16 - 21:54
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use FOS\OAuthServerBundle\Entity\Client as BaseClient;
use Symfony\Component\Serializer\Annotation\Groups;
use Dunglas\ApiBundle\Annotation\Iri;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Client
 * @package AppBundle\Entity
 *
 * @Entity()
 * @Table(name="oauth_clients")
 */
class Client extends BaseClient
{
    /**
     * @var string the resource identify
     * @Id
     * @Column(type="string")
     * @GeneratedValue(strategy="UUID")
     * @Groups({"oauth", "admin"})
     */
    protected $id;

    /**
     * @var Organization The organization for this Client
     *
     * @ManyToOne(targetEntity="Organization")
     * @Assert\NotNull()
     * @Iri("http://schema.org/Organization")
     * @Groups({"oauth", "admin"})
     */
    protected $organization;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
    }
}