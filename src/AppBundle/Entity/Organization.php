<?php
/**
 * Created by PhpStorm.
 * User: xabier
 * Date: 11/03/16
 * Time: 20:08
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Dunglas\ApiBundle\Annotation\Iri;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * An organization such as a school, NGO, corporation, club, etc.
 *
 * @Entity()
 * @Table(name="organizations")
 * @Iri("http://schema.org/Organization")
 */
class Organization
{
    /**
     * @var string the resource identify
     *
     * @Id()
     * @Column(type="string")
     * @GeneratedValue(strategy="UUID")
     * @Groups({"default"})
     */
    private $id;

    /**
     * @var PostalAddress Physical address of the item.
     *
     * @ManyToOne(targetEntity="PostalAddress")
     * @Assert\NotNull()
     * @Iri("http://schema.org/PostalAddress")
     * @Groups({"default"})
     */
    private $address;

    /**
     * @var string The Dun & Bradstreet DUNS number for identifying an organization.
     *
     * @Column(type="string", name="duns", nullable=true)
     * @Iri("http://schema.org/duns")
     * @Groups({"default"})
     */
    private $duns;

    /**
     * @var string Email address.
     *
     * @Column(type="string", nullable=true)
     * @Assert\Email()
     * @Iri("http://schema.org/email")
     * @Groups({"default"})
     */
    private $email;

    /**
     * @var string A web page.
     *
     * @Column(type="string", name="web_page", nullable=true)
     * @Assert\Url()
     * @Iri("https://schema.org/url")
     * @Groups({"default"})
     */
    private $webPage;

    /**
     * @var string The Global Location Number
     *             (GLN, sometimes also referred to as International Location Number or ILN)
     *             of the respective organization, person, or place.
     *             The GLN is a 13-digit number used to identify parties and physical locations.
     *
     * @Column(type="string", name="global_location_number", nullable=true)
     * @Iri("http://schema.org/globalLocationNumber")
     * @Groups({"default"})
     */
    private $globalLocationNumber;

    /**
     * @var string The official name of the organization, e.g. the registered company name.
     *
     * @Column(type="string", name="legal_name")
     * @Assert\NotNull()
     * @Iri("http://schema.org/legalName")
     * @Groups({"default"})
     */
    private $legalName;


    /**
     * @var string An alias for the legalName.
     *
     * @Column(type="string", name="alternate_name", nullable=true)
     * @Iri("http://schema.org/alternateName")
     * @Groups({"default"})
     */
    private $alternateName;

    /**
     * @var string The International Standard of Industrial Classification of All Economic Activities (ISIC),
     *             Revision 4 code for a particular organization, business person, or place.
     *
     * @Column(type="string", name="isic_v4", nullable=true)
     * @Assert\NotNull()
     * @Iri("http://schema.org/isicV4")
     * @Groups({"default"})
     */
    private $isicV4;

    /**
     * @var string An associated logo.
     *
     * @Column(type="string", nullable=true)
     * @Iri("http://schema.org/logo")
     * @Groups({"default"})
     */
    private $logo;

    /**
     * @var string The Tax/Fiscal ID of the organization,
     *              e.g. the TIN in the US or the CIF/NIF in Spain.
     * @Column(type="string", name="tax_id")
     * @Assert\NotNull()
     * @Iri("http://schema.org/taxID")
     * @Groups({"default"})
     */
    private $taxId;

    /**
     * @var string The telephone number.
     *
     * @Column(type="string")
     * @Assert\NotNull()
     * @Iri("http://schema.org/telephone")
     * @Groups({"default"})
     */
    private $telephone;

    /**
     * @var string The Value-added Tax ID of the organization.
     *
     * @Column(type="string", name="vat_id", nullable=true)
     * @Iri("http://schema.org/vatID")
     * @Groups({"default"})
     */
    private $vatId;

    /**
     * @var User the access user for this organization
     *
     * @ManyToOne(targetEntity="User")
     * @Assert\NotNull()
     * @Iri("http://schema.org/User")
     * @Groups({"admin"})
     */
    private $user;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return PostalAddress
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param PostalAddress $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getDuns()
    {
        return $this->duns;
    }

    /**
     * @param string $duns
     */
    public function setDuns($duns)
    {
        $this->duns = $duns;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getWebPage()
    {
        return $this->webPage;
    }

    /**
     * @param string $webPage
     */
    public function setWebPage($webPage)
    {
        $this->webPage = $webPage;
    }

    /**
     * @return string
     */
    public function getGlobalLocationNumber()
    {
        return $this->globalLocationNumber;
    }

    /**
     * @param string $globalLocationNumber
     */
    public function setGlobalLocationNumber($globalLocationNumber)
    {
        $this->globalLocationNumber = $globalLocationNumber;
    }

    /**
     * @return string
     */
    public function getLegalName()
    {
        return $this->legalName;
    }

    /**
     * @param string $legalName
     */
    public function setLegalName($legalName)
    {
        $this->legalName = $legalName;
    }

    /**
     * @return string
     */
    public function getAlternateName()
    {
        return $this->alternateName;
    }

    /**
     * @param string $alternateName
     */
    public function setAlternateName($alternateName)
    {
        $this->alternateName = $alternateName;
    }

    /**
     * @return string
     */
    public function getIsicV4()
    {
        return $this->isicV4;
    }

    /**
     * @param string $isicV4
     */
    public function setIsicV4($isicV4)
    {
        $this->isicV4 = $isicV4;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return string
     */
    public function getTaxId()
    {
        return $this->taxId;
    }

    /**
     * @param string $taxId
     */
    public function setTaxId($taxId)
    {
        $this->taxId = $taxId;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getVatId()
    {
        return $this->vatId;
    }

    /**
     * @param string $vatId
     */
    public function setVatId($vatId)
    {
        $this->vatId = $vatId;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}
