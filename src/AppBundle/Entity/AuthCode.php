<?php
/**
 * This file (AuthCode.php) is part of the api-ripapp project.
 *
 * 2014 (c) jjbier@gamil.com.
 * Created by Xabier Fernández Rodríguez <jjbier@gmail.com>
 * Date: 30/03/16 - 22:13
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

use FOS\OAuthServerBundle\Entity\AuthCode as BaseAuthCode;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class AuthCode
 * @package AppBundle\Entity
 *
 * @Entity()
 * @Table(name="oauth_auth_codes")
 */
class AuthCode extends BaseAuthCode
{
    /**
     * @var string the resource identify
     * @Id
     * @Column(type="string")
     * @GeneratedValue(strategy="UUID")
     * @Groups({"oauth", "admin"})
     */
    protected $id;


    /**
     * @ManyToOne(targetEntity="Client")
     * @JoinColumn(nullable=false)
     * @Groups({"oauth"})
     */
    protected $client;


    /**
     * @ManyToOne(targetEntity="User", fetch="EXTRA_LAZY")
     * @JoinColumn(onDelete="CASCADE")
     * @Groups({"oauth"})
     */
    protected $user;

    /**
     * AuthCode constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    
    /**
     * The token as string
     * @return string  the token string
     */
    public function __toString()
    {
        return $this->getToken();
    }

}