<?php
/**
 * Created by PhpStorm.
 * User: xabier
 * Date: 24/02/16
 * Time: 20:42
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Validator\Constraints as Assert;
use Dunglas\ApiBundle\Annotation\Iri;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Mourning.
 * The expression of deep sorrow for someone who has died, typically involving following
 * certain conventions such as wearing black clothes.
 *
 * @package AppBundle\Entity
 *
 * @Entity()
 * @Table(name="mournings")
 */
class Mourning
{
    /**
     * @var string the resource identify
     * @Id
     * @Column(type="string")
     * @GeneratedValue(strategy="UUID")
     * @Groups({"default"})
     */
    private $id;

    /**
     * @var \DateTime A date value in ISO 8601 date format.
     * @Column(type="datetime" )
     * @Assert\NotNull()
     * @Assert\DateTime()
     * @Iri("https://schema.org/Date")
     * @Groups({"default"})
     */
    private $date;

    /**
     * @var string Last words
     * @Column(type="string", nullable=true)
     * @Assert\NotBlank()
     * @Iri("https://schema.org/CommunicateAction")
     * @Groups({"default"})
     */
    private $speech;

    /**
     * @var Place The Place
     *
     * @ManyToOne(targetEntity="Place")
     * @Assert\NotNull()
     * @Iri("http://schema.org/Place")
     * @Groups({"default"})
     */
    private $place;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getSpeech()
    {
        return $this->speech;
    }

    /**
     * @param string $speech
     */
    public function setSpeech($speech)
    {
        $this->speech = $speech;
    }

    /**
     * @return Place
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param Place $place
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }
}
