<?php
/**
 * This file (DefaultController.php) is part of the api-ripapp project.
 *
 * 2014 (c) jjbier@gamil.com.
 * Created by Xabier Fernández Rodríguez <jjbier@gmail.com>
 * Date: 16/03/16 - 20:53
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace AppBundle\Controller;

use Dunglas\ApiBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends ResourceController
{
    public function cgetAction(Request $request)
    {
        $response = parent::cgetAction($request);
        return $response;
    }
}