<?php
/**
 * Created by PhpStorm.
 * User: xabier
 * Date: 11/02/16
 * Time: 23:57
 */

namespace AppBundle\DependencyInjection;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class AppExtension extends Extension
{

    /**
     * Loads a specific configuration.
     *
     * @param array $configs An array of configuration values
     * @param ContainerBuilder $container A ContainerBuilder instance
     *
     * @throws \InvalidArgumentException When provided tag is not defined in this extension
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loaderApiResources = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config/api.resources'));

        $loaderApiResources->load('postal_address.xml');
        $loaderApiResources->load('geo_coordinates.xml');
        $loaderApiResources->load('resource.place.xml');
        $loaderApiResources->load('resource.place.xml');
        $loaderApiResources->load('resource.organization.xml');
        $loaderApiResources->load('resource.burial.xml');
        $loaderApiResources->load('resource.mourning.xml');
        $loaderApiResources->load('resource.person.xml');
        $loaderApiResources->load('person_contact.xml');
        $loaderApiResources->load('contant_type.xml');
        $loaderApiResources->load('user.xml');

    }
}