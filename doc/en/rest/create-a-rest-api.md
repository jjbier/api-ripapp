# Create a production ready rest api.

Here we suppose you’re already familiar with what a REST API exactly means, and I really mean the emphasis in exactly

for this I urge you to read these links:

* http://martinfowler.com/articles/richardsonMaturityModel.html
* http://blog.steveklabnik.com/posts/2011-07-03-nobody-understands-rest-or-http
* https://github.com/interagent/http-api-design
* http://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api

An API is made to resist the test of time, so you can really afford to 
spend one or two hours to carefully read these links.

