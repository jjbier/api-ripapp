#!/bin/bash
rm -fr var/cache/*
rm -fr var/logs/*

sudo setfacl -R -m u:www-data:rwx -m u:`whoami`:rwx var/cache var/logs
sudo setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx var/cache var/logs

